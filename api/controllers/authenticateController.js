/**
 * Created by josefgasewicz on 06/10/2016.
 */
var User = require('../models/user');
var jwt = require('jsonwebtoken');

module.exports.verifyUser = function(req, res, next) {

  var token = req.body.token || req.query.token || req.headers['x-access-token'];
  if(!token)
    return res.status(500).send({error: 'Failed authentication'});
  if(token) {
    jwt.verify(token, 'secretShit', function(err, decoded){
      if(err) {
        return;
      }
      req.user = decoded.user;

      next();
    });
  }
};
