/**
 * Created by josefgasewicz on 14/10/2016.
 */
var Campaign = require('../models/campaign');
var Asset = require('../models/asset');
var SubCampaign = require('../models/subCampaign');
var mongoose = require('mongoose');

///=====================================================================
///=====================================================================
/// Campaign
///=====================================================================
///=====================================================================
module.exports.createCampaign = function(req, res, next) {

    var campaign = new Campaign({
      archive: req.body.archive,
      managed: req.body.managed,
      manager: req.body.manager,
      name:    req.body.name,
      placeholder: req.part.name
    });

    campaign.save(function(err){
      if(err)
        return res.send(err);
      res.json({success: 'Campaign saved!'}); // TODO error here
    });
};


module.exports.checkCampaignName = function(req, res, next) {
  Campaign.findOne({name: req.body.name}, function(err, campaign){
    if(campaign)
      return res.status(500).json({error: 'Campaign name already exists: error: ' + err});
    next();
  });
};



module.exports.getCampaigns = function(req, res){

  SubCampaign.find({campaignId : req.params.campaign_id})
    .populate('assets')
    .exec(function(err, subCampaigns) {
      res.json(subCampaigns);
    });

};
///=====================================================================
///=====================================================================
/// SubCampaign
///=====================================================================
///=====================================================================

/**
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.checkSubCampaignNameExists = function(req, res, next) {

  SubCampaign.findOne({name: req.body.name}, function(err, subCampaign) {
    if(subCampaign)
      return res.status(500).json({error: 'SubCampaign name already exists: error: ' + err});
    next();
  });
};
/**
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.checkCampaignNameExists = function(req, res, next) {
  Campaign.findOne({_id: req.params.campaign_id}, function(err, campaign){
    if(!campaign)
      return res.status(500).json({error: 'No campaign avaiable with this ref: error: ' + err});
    next();
  });
};

/**
 *
 * @param req
 * @param res
 * @param next
 */
module.exports.createSubCampaign = function(req, res, next) {

  var subCampaign = new SubCampaign({
    name: req.body.name,
    campaignId: req.params.campaign_id
  });

  subCampaign.save(function(err) {
    if(err) return;
    res.json({success: 'subCampaign Saved!'});
  });

};

/**
 *
 * @param req
 * @param res
 */
module.exports.getAllCampaigns = function(req, res) {
  Campaign.find(function(err, campaign){
    if(err) return;
    res.json(campaign);
  });

};

/**
 *
 * @param req
 * @param res
 */
module.exports.getSubCampaignsOrCampaign = function(req, res) {

  SubCampaign.find({campaignId : req.params.campaign_id}, function(err, subCampaigns){
    if(err) return;
    res.json(subCampaigns);
  })
};

module.exports.getFullCampaignObj = function(req, res) {

};

///=====================================================================
///=====================================================================
/// ASSETS
///=====================================================================
///=====================================================================

module.exports.createNewAsset = function(req, res) {

  var asset = new Asset({
    assetType : req.body.assetType,
    name: req.body.name,
    placeholder: req.part.name,
    size: req.body.size
  });
  asset.save();

  SubCampaign.findOne({ _id: req.params.subCampaign_id}, function(err, subCampaign) {
    subCampaign.assets.push(asset._id);
    subCampaign.save(function(err) {
      if(err) return;
      res.json({success: 'asset saved!'});
    });
  });
};


//Favorite.update( {cn: req.params.name}, { $pullAll: {uid: [req.params.deleteUid] } } )


module.exports.deleteAsset = function(req, res) {
  Asset.remove({_id: req.params.asset_id}).exec().then(function(asset) {
    SubCampaign.find({_id : req. params.subCampaign_id}).remove().exec();
    this.model('SubCampaign').remove({assets: req.params.asset_id});
  });

};
//this.model('Assignment').remove({ person: this._id }, next);

//User.remove({_id: alex._id}).exec().then(function(user){
//  ....Post.find({ postedBy : alex._id }).remove().exec();
//  ....Model.find({ createdBy : alex._id }).remove().exec();
//  ..........
//}
