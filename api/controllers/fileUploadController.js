/**
 * Created by josefgasewicz on 13/10/2016.
 */
'use strict';
const mongoose = require('mongoose'),
    _ = require('lodash'),
    Grid = require('gridfs-stream'),
    Campaign = require('../models/campaign'),
    config = require('../config');
    Grid.mongo = mongoose.mongo;
var gfs = Grid(mongoose.connection.db);

module.exports.create = function(req, res, next) {



  console.log(req.files);

  var part = req.files.file;

  var writeStream = gfs.createWriteStream({
    filename: part.name,
    mode: 'w',
    content_type: part.mimetype
  });

  writeStream.write(part.data);
  writeStream.end();
  req.part = part;
  // next();
  res.json({success: ' File Uploads',
            file : {
              filename: part.name,
              content_type: part.mimetype
            }});
};

module.exports.read = function(req, res) {

    gfs.files.find({ filename: req.params.filename }).toArray(function(err, files){

      if(files.length===0) {
        return res.status(400).send({
          message: 'File not found'
        });
      }

      res.writeHead(200, {'Content-Type': files[0].contentType});

      var readstream = gfs.createReadStream({
        filename: files[0].filename
      });

      readstream.on('data', function(data) {
        res.write(data);
      });

      readstream.on('end', function() {
        res.end();
      });

      readstream.on('error', function(err) {
        console.log('an error occured!', err);
        throw err;
      });

    });
};

