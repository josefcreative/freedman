/**
 * Created by josefgasewicz on 05/10/2016.
 */
var User = require('../models/user');
var jwt = require('jsonwebtoken');

module.exports.getLoginPage = function(req, res) {
    res.json('hi there!');
};

module.exports.loginAuth = function(req, res) {

    User.findOne({ email : req.body.email }, function(err, user){
        if(err) {
          res.status(500).json({error: err});
          return;
        }
        if(!user){
          res.status(500).json({error: err});
          return;
        }
        if(user){
            if(user.password !== req.body.password) {
              res.status(500).json({error: err});
              return;
            }
            var token = jwt.sign(
              {
                user : user
              }
              , 'secretShit');
            res.json({
              user : {
              id: user._id,
              name: user.name,
              surname: user.surname,
              email: user.email,
              market: user.market,
              company: user.company
            },
            token: token
            });
        }
    });
};

module.exports.logOut = function(res, req){

};
