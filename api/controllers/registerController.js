/**
 * Created by josefgasewicz on 09/10/2016.
 */
var User = require('../models/user');

module.exports.registerUser = function(req, res) {

  if(!req.body.name) {
    res.json({message: 'Please re enter your name'});
    return;
  }
  if(!req.body.surname) {
    res.json({message: 'Please re enter your surname'});
    return;
  }
  if(!req.body.password) {
    res.json({message: 'Please re enter your password'});
    return;
  }

  if(!req.body.email) {
    res.json({message: 'Please re enter your email'});
    return;
  }

  if(!req.body.company) {
    res.json({message: 'Please re enter the Company field'});
    return;
  }

  var user = new User({
    name: req.body.name,
    surname: req.body.surname,
    email : req.body.email,
    password: req.body.password,
    company: req.body.company,
    market: req.body.market
  });
  user.save(function(err) {
    if(err) {
      res.json('There was a problem with one or more of the fields: ' + err);
      return;
    }
    res.json({ success: true });
  });

};
