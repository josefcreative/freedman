var User = require('../models/user');

module.exports.getSingleUser = function(req, res) {

  User.findById(req.params.user_id, function(err, user) {
    if (err) {
      res.json({message: 'You need to be an Admin to access this data: ' + err});
      return;
    }
    if(req.params.user_id !== req.user._id)
      return;
    res.json(user);
  });
};

module.exports.userSessionCheck = function(req, res) {
  User.findById(req.user._id, function(err, user){
    if(err)
      return;
    res.json(user);
  });
};

module.exports.getAllUsers = function(req, res) {


  if(!req.user.admin) {
    res.json({message: 'You need to be an Admin to access this data: '});
    return;
  }

  User.find(function(err, users){
    if(err)
      return;
    res.json(users);
  });
};


