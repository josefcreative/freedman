/**
 * Created by josefgasewicz on 13/10/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AssetSchema = new Schema({
  assetType : String,
  name: String,
  placeholder: String,
  size: String
});

module.exports = mongoose.model('Asset', AssetSchema);
