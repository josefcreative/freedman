var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Asset = require('./asset');

var CampaignSchema = new Schema({
  archive: Boolean,
  managed: Boolean,
  manager: {
    type: String
  },
  name : {
    type: String,
    required: true,
    unique: [true, 'Campaign name already exists']
  },
  placeholder: String
});

module.exports = mongoose.model('Campaign', CampaignSchema);
