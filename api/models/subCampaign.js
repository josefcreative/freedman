/**
 * Created by josefgasewicz on 18/10/2016.
 */
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Asset = require('./asset');

var SubCampaigneSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  campaignId: {
    type: String,
    required: true
  },
  assets: [{type:mongoose.Schema.Types.ObjectId, ref: 'Asset'}]
});

module.exports = mongoose.model('SubCampaign', SubCampaigneSchema);
