/**
 * Created by josefgasewicz on 04/10/2016.
 */
var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    name: {
      type: String,
      required: true,
      min: [3, 'Your name is too short!'],
      max: [10,'Your name is too long!']
    },
    surname: {
      type: String,
      required: true,
      min: [3, 'Your surname is too short!'],
      max: [10,'Your surname is too long!']
    },
    password: {
      type: String,
      min: [8, 'Your password is too short!'],
      max: [25,'Your password is too long!']
    },
    hash: String,
    salt: String,
    email : {
      type: String,
      unique: [true, 'this email is already taken!'],
      required: true,
      minlength: [6, 'Your email is too short!'],
      maxlength: [25, 'Your email is too long!']
    },
    company: {
      type: String,
      min: [3, 'Company name is too short!'],
      max: [10,'Company name is too long!']
    },
    market: {
      type: String,
      min: [3, 'Market name is too short!'],
      max: [20,'Market name is too long!']
    },
    admin: Boolean,
    allowed: Boolean


});

module.exports = mongoose.model('User', UserSchema);

