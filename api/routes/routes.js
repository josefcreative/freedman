
var loginCtrl = require('../controllers/loginController');
var authCtrl = require('../controllers/authenticateController');
var userCtrl = require('../controllers/userController');
var registerCtrl = require('../controllers/registerController');
var uploadCtrl = require('../controllers/fileUploadController');
var campaignCtrl = require('../controllers/campaignController');

module.exports = function(router) {
    router.get('/', loginCtrl.getLoginPage);
    // register / login
    router.post('/register', registerCtrl.registerUser);
    router.post('/login', loginCtrl.loginAuth);
    /// User routes
    router.get('/user', authCtrl.verifyUser, userCtrl.userSessionCheck);
    router.get('/users', authCtrl.verifyUser, userCtrl.getAllUsers);
    router.get('/users/:user_id', authCtrl.verifyUser, userCtrl.getSingleUser);
    // uploads
    router.get('/uploads/:filename', uploadCtrl.read); // TODO authentication
    // router.get('/campaigns', authCtrl.verifyUser, campaignCtrl.getCampaigns);


    router.get('/campaign/:campaign_id',
      campaignCtrl.getCampaigns);

    /**
     * Create a Campaign
     */
    router.post('/campaign',
      uploadCtrl.create,
      campaignCtrl.checkCampaignName,
      campaignCtrl.createCampaign
    );

    /**
     * Create a subcampaign and add the Campaign ID
     */
    router.post('/subcampaign/:campaign_id',
      campaignCtrl.checkSubCampaignNameExists,
      campaignCtrl.checkCampaignNameExists,
      campaignCtrl.createSubCampaign
    );

    /**
     * Create a new Asset & add the SubCampaign ID
     */
    router.post('/asset/:subCampaign_id',
      uploadCtrl.create,
      campaignCtrl.createNewAsset
    );


    /**
     * Get assets of subcampaigns, subcampaigns, and master campaign
     *
     */
      router.get('/campaignObj/:campaign_id', campaignCtrl.getFullCampaignObj);

    /**
     * Delete one asset
     */
      router.delete('/asset/:subCampaign_id/:asset_id',
        campaignCtrl.deleteAsset
      );

    /**
     * drag and drop tests
     */
    router.post('/drop', uploadCtrl.create);

};
