/**
 * Created by josefgasewicz on 04/10/2016.
 */
// =======================
// get the packages ============
// =======================
const express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    busboyBodyParser = require('busboy-body-parser'),
    mongoose = require('mongoose');

// DATABASE
var config = require('./api/config');
mongoose.connect(config.database);
mongoose.connection.once('open', function(){
  console.log('mongoDB connected');
});
var conn = mongoose.createConnection(config.database);

// middleware
app.use(function(req, res, next){
  next();
});
app.use(busboyBodyParser()); // add uploaded files to req.files
app.use(bodyParser.urlencoded({ limit: '700mb', extended: false}));
app.use(bodyParser.json());
app.use(morgan('dev')); // log requests

app.use(function (req, res, next) {

    // Website you wish to allow to connect
    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'multipart/form-data,x-access-token,X-Requested-With,content-type,X-ACCESS_TOKEN,Access-Control-Allow-Origin,Authorization,Origin,Content-Range,Content-Disposition,Content-Description');

    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);

    // Pass to next layer of middleware
    next();
});


var router = express.Router();

require('./api/routes/routes')(router);
app.use('/api', router);


app.listen(3000, function(){
    console.log('api running on 3000');
});




