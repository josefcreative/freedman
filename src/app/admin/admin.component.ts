import { Component, OnInit } from '@angular/core';
import {HttpService} from "../services/http.service";
import {Campaign} from "../classes/campaign";


@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  private campaign:Campaign[] = [];

  constructor() { }

  ngOnInit() {
  }

}
