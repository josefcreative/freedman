/**
 * Created by josefgasewicz on 31/10/2016.
 */
import { Routes } from '@angular/router';

import {DashboardComponent} from "./dashboard/dashboard.component";
import {RequestComponent} from "./request/request.component";
import {SettingsComponent} from "./settings/settings.component";
import {UploadComponent} from "./upload/upload.component";

import {AuthGuard} from "../_guards/auth.guard";
import {AdminComponent} from "./admin.component";

export const ADMIN_ROUTES: Routes = [
  { path: '', component: DashboardComponent},
  { path: 'dashboard', component: DashboardComponent},
  { path: 'requests', component: RequestComponent},
  { path: 'settings', component: SettingsComponent},
  { path: 'upload', component: UploadComponent }
];
