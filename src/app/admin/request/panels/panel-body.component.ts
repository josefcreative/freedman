import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-panel-body',
  template: `
    <div>
      <div class="campaign-request-details">

        <div class="row">
          <div class="col-xs-6 col-md-3" *ngFor="let sub of order.requests">
            <b><span class="blue-caps">{{sub.title}}</span> - Display</b>
            <div *ngFor="let banners of sub.banners">
              {{banners.height}} x {{banners.height}}
            </div>
          </div><!--/.col-md-3-->

          <div class="col-xs-6 col-md-3" *ngFor="let sub of order.requests">
            <b><span class="blue-caps">{{sub.title}}</span> - Video</b>
            <div *ngFor="let banners of sub.videos">
              {{banners.height}} x {{banners.height}}
            </div>
          </div><!--/.col-md-3-->
        </div><!--/.row-->

        <div class="campaign-request-details-footer">
          <ul>
            <li>Deadline <b>{{order?.deadline}}</b></li>
            <li>Campaign live date <b>{{order?.live}}</b></li>
            <li>Localisation need:
              <b>
                <span class="red-caps" *ngIf="order?.localization">
                  Yes
                </span>
                <span class="red-caps" *ngIf="!order?.localization">
                  No
                </span>
              </b>
            </li>
            <li>Translation need:
              <b>
                <span class="orange-caps" *ngIf="order?.translation">
                  Yes
                </span>
                <span class="orange-caps" *ngIf="!order?.translation">
                  No
                </span>
              </b>
            </li>
          </ul>

          <ul>
            <li>Translation <a href="" class="download">Download</a></li>  <!-- TODO-->
            <li>Specs <a href="" class="download">Download</a></li>  <!-- TODO-->
          </ul>
        </div><!--/.campaign-request-details-footer-->

      </div><!--/.campaign-request-details-->
    </div><!--/.*ngIf-->
  `,
  styles: []
})
export class PanelBodyComponent implements OnInit {

  @Input() order;

  constructor() { }

  ngOnInit() {
  }

}
