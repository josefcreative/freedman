import { Component, OnInit, Output, Input, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-panel-footer',
  template: `
    <div class="campaign-request-footer clearfix">
      <button type="button" class="btn archive pull-left">Archive Job</button>

      <div *ngIf="!showBody">
        <div role="button" (click)="showBody=true;showPanelBody(order.id);" class="show-toggle pull-right">
          <i class="fa fa-plus-circle" aria-hidden="true"></i>
          <span>Show More</span>
        </div>
      </div>

      <div *ngIf="showBody">
        <div role="button" (click)="showBody=false;showPanelBody(order.id);" class="show-toggle pull-right">
          <i class="fa fa-minus-circle" aria-hidden="true"></i>
          <span>Show Less</span>
        </div>
      </div>

    </div>
  `,
  styles: []
})
export class PanelFooterComponent implements OnInit {

  showBody:boolean=false;
  @Input() order:any;
  @Output() onShowMore = new EventEmitter<any>();

  constructor() {
  }

  showPanelBody(id) {
      this.onShowMore.emit(
        {
          display: this.showBody,
          orderId : id
        }
      );
  }

  ngOnInit() {

  }

}
