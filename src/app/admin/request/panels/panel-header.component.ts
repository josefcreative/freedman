import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-panel-header',
  template: `
    <table class="campaign-request-header">
      <tr>
        <th>Campaign No.</th>
        <th>Campaign name</th>
        <th>User</th>
        <th>Market</th>
        <th>Requested</th>
        <th>Deadline</th>
        <th>Status</th>
      </tr>
      <tr>
        <td>{{order?.campaign}}</td>
        <td>{{order?.title}}</td>
        <td>{{order?.username}}</td>
        <td>{{order?.market}}</td>
        <td>{{order?.created}}</td>
        <td>{{order?.deadline}}</td>
        <td><span class="red-caps">{{order?.status}}</span></td>
      </tr>
    </table>
  `,
  styles: []
})
export class PanelHeaderComponent implements OnInit {

  @Input() order;

  constructor() { }

  ngOnInit() {
  }

}
