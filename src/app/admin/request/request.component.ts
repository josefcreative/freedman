import { Component, OnInit, Output } from '@angular/core';
import {OrdersService} from "../../services/orders.service";
import {Order} from "../../classes/order";
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-request',
  templateUrl: './request.component.html',
  styleUrls: ['./request.component.css']
})
export class RequestComponent implements OnInit {
  private ordersCollection;
  private orders;
  private subCampaigns;
  private showBody:any;

  constructor(private httpService:HttpService) { }

  ngOnInit() {
    this.getOrdersCollection();
    console.log(this.ordersCollection);
  }

  getOrdersCollection() {
    this.httpService.getMethod('order','')
      .subscribe(
        data => {
          console.log(data.return);
          this.ordersCollection = data.return;
        },
        error => {
          console.error(error);
        }
      );
  }
  onClickShowMore(value) {
    this.showBody = value;
    console.log(this.showBody);
  }


}
