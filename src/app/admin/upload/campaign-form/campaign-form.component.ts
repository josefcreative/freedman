import {Component, OnInit, Input} from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import { DragDropDirective } from '../../../directives/drag-drop.directive';
import {FileUploadService} from "../../../services/file-upload.service";
import {HttpService} from "../../../services/http.service";
import {Campaign} from "../../../classes/campaign";

@Component({
  selector: 'app-campaign-form',
  templateUrl: './campaign-form.component.html',
  styleUrls: ['./campaign-form.component.css']
})
export class CampaignFormComponent implements OnInit {

  @Input() campaign;
  private formData:FormGroup;
  private imageData:any[]=[];
  private campaignData:any={};
  private imageType:string;
  private inputs:any={};
  private token:string;

  private EnglishMessages = [['Drag and Drop','or Click to Browse'],['Click to Browse',''], ['Drop image to','start uploading']];
  private PortugueseMessages = [['Arraste e Largue','ou Clique para Escolher'],['Clique para Escolher',''], ['Large imagem para','iniciar transferencia']];
  private ErrorMessages = [['Undefined','Error'],['Undefined','Error'],['Undefined','Error']];


  constructor(private httpService:HttpService) {
    this.formData = new FormGroup({
      'title': new FormControl('', Validators.required),
      'thumbnail': new FormControl('', Validators.required),
      'background': new FormControl('', Validators.required)
    });
  }

  ngOnInit() {

  }

  Test(event,instance)
  {
    if(!event.ok)
    {
      this.ErrorMessages[instance][0] = 'Serverside';
      this.ErrorMessages[instance][1] = 'Failure';
    }
    else
    {

      if(instance === 0){
        this.imageType = 'thumbnail';
        this.inputs.thumbnail = event.filename;
      } else {
        this.imageType = 'background';
        this.inputs.background = event.filename;
      }
      event.type = this.imageType;
      this.imageData.push(event);
    }
  }


  onCreateCampaign(e) {
    e.preventDefault();
    console.log(this.imageData);
    this.campaignData = new FormData;
    this.campaignData.append('title', this.formData.value.title);
    for(var i=0; i<this.imageData.length; i++) {
      if(this.imageData[i].type === 'thumbnail') this.campaignData.append('thumbnail', this.imageData[i].filename);
      if(this.imageData[i].type === 'background') this.campaignData.append('background', this.imageData[i].filename);
    }
    this.httpService.postMethod('campaign', '', this.campaignData)
      .subscribe(
        data => {
          console.log(data);
        }
      )
  }



}
