import { Component, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { FileUploadService } from  '../../../services/file-upload.service';
import { Observable, Observer } from 'rxjs/Rx';

@Component({
  selector: 'app-drop-zone',
  template: `

		<div class="dropzone" [class.pending]="ComponentStatus == 1 && Interaction == 0 && !showPlaceholder" [class.over]="Interaction == 1 || Interaction == 2" (mouseenter)="EventController($event)" (mouseleave)="EventController($event)">
      <div *ngIf="ComponentStatus != 4" class="info">
        <div class="label">{{ InteractionMessages[Interaction][0] }}</div>
        <div class="label">{{ InteractionMessages[Interaction][1] }}</div>
      </div>
      <div *ngIf="ComponentStatus == 4" class="info error">
        <div class="label">{{ ErrorMessages[0] }}</div>
        <div class="label">{{ ErrorMessages[1] }}</div>
      </div>
      <img [ngClass]="{'placeholder' : !showPlaceholder}" [src]="placeHolder || imagePlaceholder" [style.display]="ComponentStatus > 0 && ComponentStatus < 4 && Interaction == 0 ? 'block':'none'" />
      
      <div class="progressbar" [style.margin-left.%]="image.progress - 100" [style.display]="ComponentStatus > 0 && ComponentStatus < 4 && Interaction == 0 ? 'block':'none'">
        <img class="placeholder" [src]="imagePlaceholder" [style.margin-left.%]="100 - image.progress"/>
      </div><!--/.progressbar-->
      
      <input #fif type="file" name="file" [title]="InteractionMessages[1][0]+' '+InteractionMessages[1][1]" accept="image/*" (change)="EventController($event)" [style.display]="ComponentStatus != 2 ? 'block':'none'" [style.z-index]="Interaction == 1 ? '10' : '0'">
      <div class="dragCollect" (drop)="EventController($event)" (dragover)="$event.preventDefault()" (dragenter)="EventController($event)" (dragleave)="EventController($event)" [style.display]="ComponentStatus != 2 ? 'block':'none'"></div>
    </div>
  `,
  styleUrls: ['./drop-zone.component.css']
})
export class DropZoneComponent implements OnInit {
  @ViewChild("fif") fif;
  @Input('messages') InteractionMessages;
  @Input('errors') ErrorMessages;
  @Output('return') UploadedImage = new EventEmitter();
  @Input() campaign;
  @Input() placeHolder;
  showPlaceholder=false;

  private ComponentStatus: number = 0;
  private Interaction: number = 0;
  private CurrentError: number = 0;

  private imagePlaceholder: string = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAMAAABFaP0WAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALMw9IgAAAEAdFJOU////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wBT9wclAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4wLjEyQwRr7AAAAA5JREFUGFdj+P+f4f9/AAv6A/2W1g4mAAAAAElFTkSuQmCC';
  private image = {
    'status':false,
    'progress': 0,
    'filename': null,
    'file': null,
    'return': null
  };

  constructor() {}

  ngOnInit() {
    if(typeof this.InteractionMessages == 'undefined')
      this.InteractionMessages = [['Drag and Drop','or Click to Browse'],['Click to Browse',''], ['Drop image to','start uploading']];
    if(typeof this.ErrorMessages == 'undefined')
      this.ErrorMessages = ['Unknown','Error'];
    this.getPlaceHolder();
  }

  GetImageURL(file) {
    let TObs$: Observable<any> = Observable.create(observer => {

      let reader: FileReader = new FileReader();

      reader.onload = (event) => {
        observer.next(event.target);
        observer.complete();
      };

      reader.readAsDataURL(file);
    });

    TObs$.subscribe(
      data => {
        this.imagePlaceholder = data.result;
      }
    );
  }


  UploadFirst()
  {
    this.image.progress = 0;
    FileUploadService.UploadFile(this.image.file).subscribe(
      data => {
        this.ProcessCompleteFile(data);
      });
  }

  ProcessCompleteFile(result)
  {
    this.image.progress = 100;
    if(result[0] == 200)
      try
      {
        let parsedResult =JSON.parse(result[1]);
        this.UploadedImage.emit(parsedResult.return);
        this.ComponentStatus = 3;
      }catch(e){
        this.ComponentStatus = 4;
        this.UploadedImage.emit({"ok":false,"error":"JSON Parse Failed","width":null,"height":null,"filename":''});
      }
    else
    {
      this.ComponentStatus = 4;
      this.UploadedImage.emit({"ok":false,"error":"Server Connection Failed","width":null,"height":null,"filename":''});
    }
  }

  UpdateControl()
  {
    if(this.ComponentStatus < 2)
    {
      this.image.status = true;
      if(FileUploadService.Available())
      {
        this.ComponentStatus = 2;
        this.UploadFirst();
      }
      else
      {
        this.ComponentStatus = 1;
        setTimeout(() => { this.UpdateControl() }, 250);
      }
    }

    if(this.ComponentStatus == 2)
    {
      this.image.progress = FileUploadService.GetProgress();
      setTimeout(() => { this.UpdateControl() }, 250);
    }
  }


  EventController(event)
  {
    if(this.ComponentStatus == 4)
      this.ComponentStatus = 0;

    switch (event.type)
    {
      case 'mouseenter':
        if(this.Interaction == 0 && this.ComponentStatus != 2)
          this.Interaction = 1;
        break;
      case 'mouseleave':
        if(this.Interaction == 1)
          this.Interaction = 0;
        break;
      case 'dragenter':
        if(this.Interaction == 0 && this.ComponentStatus != 2)
          this.Interaction = 2;
        break;
      case 'dragleave':
        if(this.Interaction == 2)
          this.Interaction = 0;
        break;
      case 'drop':
      case 'change':
        if(this.ComponentStatus == 3)
        {
          this.ComponentStatus = 0;
          this.UploadedImage.emit({"ok":true,"error":null,"width":null,"height":null,"filename":''});
        }

        event.preventDefault();
        event.stopPropagation();

        if(event.type == 'drop')
          var SelectedFiles = event.dataTransfer.files;
        else
          var SelectedFiles = event.target.files;


        var filename = SelectedFiles[0].name;
        if(typeof filename != 'undefined' && filename.length > 4)
          this.image = {'status':false, 'progress': 0, 'filename':filename, 'file':SelectedFiles[0], 'return':null};


        this.GetImageURL(SelectedFiles[0]);
        this.fif.nativeElement.value = '';
        this.Interaction = 0;
        this.UpdateControl();

        break;
    }
  }

  getPlaceHolder() {
    if(typeof this.campaign!=='undefined' && this.campaign!==null ) {
      this.showPlaceholder=true;
     // this.imagePlaceholder = this.campaign.thumbnail;
      this.ComponentStatus = 1;
      this.Interaction = 0;
    } else {
      this.showPlaceholder=false;
      this.ComponentStatus = 0;
      this.Interaction = 0;
     // this.imagePlaceholder = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAIAAAACCAMAAABFaP0WAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAMAUExURQAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAALMw9IgAAAEAdFJOU////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////wBT9wclAAAACXBIWXMAAA7DAAAOwwHHb6hkAAAAGXRFWHRTb2Z0d2FyZQBwYWludC5uZXQgNC4wLjEyQwRr7AAAAA5JREFUGFdj+P+f4f9/AAv6A/2W1g4mAAAAAElFTkSuQmCC';
    }
  }



}
