import {Component, OnInit, Input, Output, EventEmitter} from '@angular/core';

import {SubCampaignUploadService} from "../../../services/sub-campaign-upload.service";

@Component({
  selector: 'app-progress-bar',
  template: `
   <div [hidden]="!upLoadComplete">
    Filename: {{subFileData?.name}}
    Product: {{subFileData?.product}}
  </div>

  <div class="progress" [hidden]="progressVisible">

    <div class="progress-bar progress-bar-striped"  [style.width.%]="progressValue" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">
      <span>{{progressValue}}% Complete</span>
    </div><!--/.progress-bar-->
  </div><!--/.progress-->
  
  
  <div class="dropdown">
  <button (click)="dropDown=!dropDown;" class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
    {{productSelection}}
    <span class="caret"></span>
  </button>
  <div [hidden]="dropDown" class="dropdown-menu" aria-labelledby="dropdownMenu1">
    
    <div *ngFor="let product of productsArray" class="radio">
  <label (click)="productSelection=product;
  addProductToAsset(product);
  subUploadService.addProduct(subFileData?.name, product, subUploadService.subCollection); dropDown=!dropDown">
    <input type="radio" name="optionsRadios" id="optionsRadios2" value="option2">
    {{product}}
  </label>
</div>

  
  </div><!--./btn-->
</div><!--./dropdown-->


  
  <div [hidden]="!upLoadComplete" class="remove-zip pull-right">Remove Zip
    <i class="fa fa-times" aria-hidden="true" (click)="subUploadService.removeItemFromCollection(subFileData, fileArray)"></i></div>
  <div class="progress-container clearfix">

    <!--<div [hidden]="!errors.subcampaign.uploadError" class="upload-error alert alert-danger">-->
      <!--Oops! Find out <a class="alert-link">what went wrong?</a>-->
    <!--</div>-->

  </div>
  `,
  styles: [`
    [hidden] {
      display: block !important;
    }
`]
})
export class ProgressBarComponent implements OnInit {
  @Input() errors;
  @Input() progressValue;
  @Input() upLoadComplete;
  @Input() subFileData;
  @Input() progressVisible;
  @Input() fileArray;
  @Input() productsArray;
  private dropDown:boolean=false;
  private productSelection:string = 'Select Product';

  constructor(private subUploadService:SubCampaignUploadService) { }

  ngOnInit() {
  }

  private addProductToAsset(product):void {
    this.subFileData.product=product;
  }
}
