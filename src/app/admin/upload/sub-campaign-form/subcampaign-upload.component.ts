import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import {SubCampaignUploadService} from "../../../services/sub-campaign-upload.service";
import {Subcampaign} from "../../../classes/subcampaign";
import {Request} from "../../../classes/request";
import {HttpService} from "../../../services/http.service";
import {Campaign} from "../../../classes/campaign";


@Component({
  selector: 'app-subcampaign-upload',
  templateUrl: './subcampaign-upload.component.html',
  styleUrls: ['../drop-zone/drop-zone.component.css']
})
export class SubcampaignUploadComponent implements OnInit {

  @Input() campaign:Campaign;
  private progressValue;
  private progressVisible=true;
  private subFileData:any;
  private upLoadComplete=false;
  private fileArray:Array<any>=[];
  private errors:any={
    subcampaign : {
      uploadError : false
    }
  };
  private productsArray:Array<any>=['Origin', 'PBM', 'Origin Access', 'EA Access'];

  private subCollection:Array<any>=[];
  private formData:any={};

  constructor(private httpService:HttpService,
              private subUploadService:SubCampaignUploadService) { }

  ngOnInit() {
    this.subCollection=this.subUploadService.subCollection;
  }

  /**
   *
   * @param file
   */
  public getFileOnDrop(file) {
    let asset:any={};
    this.fileArray.push(file);
    this.progressVisible=false;

    this.subUploadService.getObserver()
      .subscribe(progress =>{
        this.progressValue = progress;
        if(this.progressValue>=100) this.upLoadComplete=true;
      });

    let tempData = new FormData();
    tempData.append('file', file);
    this.subUploadService.upload(tempData)
      .subscribe(
        res => {
          this.subFileData = res;
          asset.filename=this.subFileData.file.filename;
          this.addAssetToCollectionOnRes(asset);
        },
        err => {
            console.log(err);
        })
  }


  addAssetToCollectionOnRes(value) {
    try {
      if(typeof value==='undefined') {
        throw ' is undefined. Could not push value to array';
      }
    }catch(err) {
      console.log(value + err);
    }finally {
      this.subUploadService.addToArrayCollection(value, this.subUploadService.subCollection);
      console.log(this.subUploadService.subCollection);
    }
  }
}
