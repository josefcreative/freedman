import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../services/http.service";
import {Campaign} from "../../classes/campaign";

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styles: [`
    .hide {
    display: block !important;
    }
`]
})
export class UploadComponent implements OnInit {

  private dropDown=false;
  private campaigns:Campaign[];
  private campaignsLoaded:boolean=false;
  private selectedCampaign:Campaign;
  private uploadForm:boolean=false;
  private newCampaign:boolean=false;

  constructor(private httpService: HttpService) {

  }

  ngOnInit() {
    this.getAllCampaigns();
  }

  private getAllCampaigns() {
    let data;
    this.httpService.getMethod('campaign', '')
      .subscribe(res => {
          data = res;
          this.campaigns=data.return;
          this.campaignsLoaded=true;
          console.log(this.campaigns);
        },
        err => {
          console.log(err);
        })
  }

  /**
   *
   * @param campaign
   */
  private campaignSelect(campaign):void {
    this.newCampaign=false;
    this.selectedCampaign = campaign;
    this.uploadForm = true;
  }

  /**
   *
   */
  private createNewCampaign():void {
    this.selectedCampaign=null;
    this.newCampaign=true;
  }


}
