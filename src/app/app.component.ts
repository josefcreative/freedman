import { Component, OnInit, Output } from '@angular/core';
import { HttpService } from './services/http.service';
import { User } from './classes/user';
import {UserService} from "./services/user.service";
import {Subscription} from "rxjs/Subscription";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  private user;
  private _sub;
  private _authentication;
  private subscription: Subscription;

  constructor(private userService:UserService, private httpService:HttpService) {
    this.user = this.userService.user;
    this.subscription = userService.userInit$.subscribe(
      user => {
        this.user = user;
      }
    );
  }

  ngOnInit() {
    this.getCampaign();
  }

  getCampaign() {
    if(this.user.id === 'undefined') return;
    this.httpService.getMethod('campaign', '')
      .subscribe(
        res => {
          if(res.result) {
            this.userService.shareUser(res.authentication.details);
          }
        }
      )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }


}
