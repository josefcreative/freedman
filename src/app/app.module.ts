import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {HttpModule, BrowserXhr} from '@angular/http';
import { RouterModule } from '@angular/router';
import {
  LocationStrategy,
  HashLocationStrategy
} from '@angular/common';

// Classes
import { AuthGuard } from './_guards/auth.guard';
import { User} from "./classes/user";
import { Order } from "./classes/order";
import { Request } from "./classes/request";
import {Subcampaign} from "./classes/subcampaign";

// Services
import { HttpService } from "./services/http.service";
import { AuthenticationService } from "./services/auth.service";
import { CampaignService } from "./services/campaign.service";
import { RequestsService } from "./services/requests.service";
import { OrdersService } from "./services/orders.service";
import {FileUploadService} from "./services/file-upload.service";
import {UserService} from "./services/user.service";
import {SubCampaignUploadService} from "./services/sub-campaign-upload.service";

// Pipes
import { SafePipe } from './pipes/safe.pipe';
import { KeysPipe } from './pipes/keys.pipe';

//Components
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { CampaignsComponent } from './campaigns/campaigns.component';
import { AdminComponent } from './admin/admin.component';
import { RegisterComponent } from './register/register.component';
import { HeaderComponent } from './header/header.component';
import { CampaignItemComponent } from './campaigns/campaign-item/campaign-item.component';
import { CampaignSelectComponent } from './campaigns/campaign-select/campaign-select.component';
import { HelpComponent } from './help/help.component';
import { ResetComponent } from './login/reset.component';
import { CamapignDetailsComponent } from './campaigns/campaign-item/campaign-detail.component';
import { ReviewItemsComponent } from './campaigns/campaign-item/review-items.component';
import { FrameComponent } from './campaigns/frame/frame.component';
import { RequestComponent } from './admin/request/request.component';
import { SettingsComponent } from './admin/settings/settings.component';
import { UploadComponent } from './admin/upload/upload.component';
import { DashboardComponent } from './admin/dashboard/dashboard.component';
import { DownloadComponent } from './campaigns/download/download.component';
import { PanelHeaderComponent } from './admin/request/panels/panel-header.component';
import { PanelBodyComponent } from './admin/request/panels/panel-body.component';
import { PanelFooterComponent } from './admin/request/panels/panel-footer.component';

/// Routing
import { AppRoutes } from './app.routes';

/// Directives
import { ImgLoaderDirective } from './directives/img-loader.directive';
import { DragDropDirective } from './directives/drag-drop.directive';
import { DropZoneComponent } from './admin/upload/drop-zone/drop-zone.component';
import { UserSettingsComponent } from './user/user-settings/user-settings.component';
import {UserRequestsComponent} from "./user/user-requests/user-requests.component";
import { CampaignItemFooterComponent } from './campaigns/campaign-item/campaign-item-footer/campaign-item-footer.component';
import { CampaignFormComponent } from './admin/upload/campaign-form/campaign-form.component';
import { AssetPanelsComponent } from './admin/upload/asset-panels/asset-panels.component';
import { VideoInputDetailComponent } from './campaigns/campaign-item/campaign-item-footer/video-input-detail.component';
import { BannerInputDetailComponent } from './campaigns/campaign-item/campaign-item-footer/banner-input-detail.component';
import { SearchTitlePipe } from './pipes/search-title.pipe';
import { SubcampaignUploadComponent } from './admin/upload/sub-campaign-form/subcampaign-upload.component';
import { ProgressBarComponent } from './admin/upload/sub-campaign-form/progress-bar.component';




@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    LoginComponent,
    CampaignsComponent,
    AdminComponent,
    RegisterComponent,
    CampaignItemComponent,
    CampaignSelectComponent,
    HelpComponent,
    ResetComponent,
    CamapignDetailsComponent,
    ReviewItemsComponent,
    FrameComponent,
    SafePipe,
    RequestComponent,
    SettingsComponent,
    UploadComponent,
    DashboardComponent,
    DownloadComponent,
    KeysPipe,
    PanelHeaderComponent,
    PanelBodyComponent,
    PanelFooterComponent,
    ImgLoaderDirective,
    DragDropDirective,
    DropZoneComponent,
    UserSettingsComponent,
    UserRequestsComponent,
    CampaignItemFooterComponent,
    CampaignFormComponent,
    AssetPanelsComponent,
    VideoInputDetailComponent,
    BannerInputDetailComponent,
    SearchTitlePipe,
    SubcampaignUploadComponent,
    ProgressBarComponent

  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    AppRoutes,
    RouterModule
  ],
  providers: [
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    HttpService,
    AuthenticationService,
    CampaignService,
    AuthGuard,
    User,
    UserService,
    RequestsService,
    Order,
    Request,
    OrdersService,
    FileUploadService,
    SubCampaignUploadService,
    Subcampaign
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
