import { Routes, RouterModule } from '@angular/router';

import {LoginComponent} from './login/login.component';
import {CampaignsComponent} from './campaigns/campaigns.component';
import {RegisterComponent} from "./register/register.component";
import {CampaignItemComponent} from "./campaigns/campaign-item/campaign-item.component";
import {HelpComponent} from "./help/help.component";


import {CAMPAIGN_ROUTES} from "./campaigns/campaigns.routes";
import { AuthGuard } from './_guards/auth.guard';
import {ADMIN_ROUTES} from "./admin/admin.routes";
import {AdminComponent} from "./admin/admin.component";
import {UserSettingsComponent} from "./user/user-settings/user-settings.component";
import {UserRequestsComponent} from "./user/user-requests/user-requests.component";

const routes: Routes = [
    { path: '', redirectTo: '/login', pathMatch: 'full'},
    { path: 'register', component: RegisterComponent},
    { path: 'login', component: LoginComponent},
    { path: 'help', component: HelpComponent},
    { path: 'admin', component: AdminComponent, canActivate: [AuthGuard], children: ADMIN_ROUTES },
    { path: 'campaigns', component:CampaignsComponent, canActivate: [AuthGuard], children: CAMPAIGN_ROUTES},
    { path: 'settings', component:UserSettingsComponent},
    { path: 'requests', component:UserRequestsComponent}
];

export const AppRoutes = RouterModule.forRoot(routes);
