import { Component, OnInit, Input, ViewChild, Output, EventEmitter} from '@angular/core';
import {RequestsService} from "../../services/requests.service";
import {Request} from "../../classes/request";
import {AuthenticationService} from "../../services/auth.service";
import {Order} from "../../classes/order";
import {Custom} from "../../classes/custom";


@Component({
  selector: 'app-campaign-detail',
  templateUrl: './campaign-detail.component.html'
})
export class CamapignDetailsComponent implements OnInit {

  @Input() subCampaign:any ={};
  @Input() showBanners:boolean;
  @Input() showVideos:boolean;
  @Input() assetType:string;
  @Input() productType:string;
  private requestItems:Request[]=[];

  @Output() clickedAsset = new EventEmitter<any>();

  onAssetClicked(item:any) {
    this.clickedAsset.emit(item);
  }

  constructor(private requestsService: RequestsService) { }

  ngOnInit() {
    this.requestItems = RequestsService.getArrayOfType(this.requestsService.requestsCollection);
  }

  /**
   *
   * @param item
   * @param checkBox
   * @param subcampaign
   * @param collection
     */
  addAssetToRequestsCollection(item:Request, checkBox:any, subcampaign:number, collection) {
    let type = item.type;
    item.subcampaign=subcampaign;
    if(checkBox.checked) {
      RequestsService.addToArrayCollection(item, type, collection);
    } else {
      this.requestsService.removeFromRequestsCollection(item);
    }
  }

  /**
   *
   * @param item
   * @param subcampaign
     */
  onAddToRequestsCollection(item:Request, subcampaign:number) {
    item.subcampaign=subcampaign;
    RequestsService.addToArrayCollection(item, item.type, this.requestsService.requestsCollection);
  }





  /**
   *  TODO create method for items to count in each subCampaign for the reviewComponent
   */
  displayInReview() {

  }
}
