import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Request} from "../../../classes/request";
import {RequestsService} from "../../../services/requests.service";

@Component({
  selector: 'app-banner-input-detail',
  template: `
 <div class="panel-foot-head">
        Custom Display dimensions
      </div>
      <div class="custom-input-container">
        <i class="italic">W:</i> <input type="text" [(ngModel)]="subCampaign.bannerWidth" name="width" placeholder="width">
        <i class="italic">H:</i> <input type="text" [(ngModel)]="subCampaign.bannerHeight" name="height" placeholder="height">
        <button (click)="bannerInput(subCampaign.bannerWidth,
                    subCampaign.bannerHeight,
                    subCampaign,
                    assetType);
                    subCampaign.bannerWidth=subCampaign.bannerHeight=null"
                class="btn add-custom">Add
        </button>
        <div *ngIf="err.length > 0" class="">
          <div class="custom-input-error">
            {{err}}
          </div>
        </div>
      </div><!--/.custom-input-container-->

      <div *ngFor="let asset of requestItems;">
        <div class="col-sm-3" *ngIf="asset?.custom &&
                    asset?.subcampaign === subCampaign.id && asset?.type + 's' === assetType && asset?.product === productType">
          {{asset.width + ' X ' + asset.height}}
          <i class="fa fa-times" aria-hidden="true" (click)="requestsService.removeFromRequestsCollection(asset);
                    asset.selected=false;"></i>
        </div>
      </div><!--/*ngFor-->
  `,
  styles: []
})
export class BannerInputDetailComponent implements OnInit {

  @Input() private productType:string;
  @Input() private assetType:string;
  @Input() private subCampaign:string;
  @Input() private requestItems:any;
  @Output() customBannerSizes:EventEmitter<any> = new EventEmitter();
  private err:string='';
  private asset:any={};


  constructor(private requestsService:RequestsService) { }

  ngOnInit() {
  }

  bannerInput(width:number, height:number, item:any, type:string) {
    this.asset = {
      width: width,
      height: height,
      item: item,
      type: type
    };
    this.customBannerSizes.emit(this.asset);
  }




}
