import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import {RequestsService} from "../../../services/requests.service";
import {AuthenticationService} from "../../../services/auth.service";
import {Request} from "../../../classes/request";

@Component({
  selector: 'app-campaign-item-footer',
  templateUrl: './campaign-item-footer.component.html'
})
export class CampaignItemFooterComponent implements OnInit {
  @Input() assetType;
  @Input() subCampaign;
  @Input() productType;
  @Input() requestItems;
  @Input() asset;
  @Output() toggleObj = new EventEmitter();
  private err:string = '';
  constructor(private requestsService:RequestsService) { }

  ngOnInit() {
  }

  /**
   * TODO refactor into campaign service as a static method
   * @param item
   * @param checkBox
   * @param subcampaign
   */
  addAssetToRequestsCollection(item:Request, checkBox:any, subcampaign:number) {
    let type = item.type;
    item.subcampaign=subcampaign;
    if(checkBox.checked) {
      RequestsService.addToArrayCollection(item, type, this.requestsService.requestsCollection);
    } else {
      this.requestsService.removeFromRequestsCollection(item);
    }
  }

  /**
   * This is for the toggle to select all assets within the subcampaign type => product type
   * uses the static method @function addToArrayCollection() to push the assets into
   * he requestItems collection.
   * @param item
   * @param toggle
   * @param collection
   * @param subcampaign
   */
  selectAllOfSubCampaign(item:any, toggle:any, subcampaign:number, collection:any) {
    let type = item.type;
    if(!toggle) {
      for(let i=0; i<item.length; i++) {
        if (item[i].product === this.productType) { // check the product type
          item[i].subcampaign = subcampaign;
          item[i].selected = true;
          RequestsService.addToArrayCollection(item[i], type, collection);
        }
      }
      return;
    }
    if(toggle) {
      for(let i=0; i<item.length; i++){
        if(item[i].product === this.productType) { // check the product type
          this.requestsService.removeFromRequestsCollection(item[i]);
          item[i].selected=false;
        }
      }
      return
    }
  };

  /**
   *
   * @param value
     */
  getInputValues(value) {
    this.err="";
    console.log('then here: ' +value.width);
    if(!AuthenticationService.checkValIsNumber(value.width) && !AuthenticationService.checkValIsNumber(value.height)) {
      this.err ="Must be a number & less than 5 digits long";
      return;
    }
    if(value.type === "banners") value.type = "banner";
    if(value.type === "videos") value.type = "video";
    let asset:Request = {
      subcampaign: value.item.id,
      product: this.productType,
      id:null,
      asset:null,
      custom:true,
      type: value.type,
      width:value.width,
      height:value.height,
      duration:value.duration,
    };
    RequestsService.addToArrayCollection(asset, asset.type, this.requestsService.requestsCollection);
    console.log(asset);
  }

}
