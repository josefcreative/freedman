import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {Request} from "../../../classes/request";
import {RequestsService} from "../../../services/requests.service";


@Component({
  selector: 'app-video-input-detail',
  template: `
 <div class="panel-foot-head">
        Custom Display dimensions
      </div>
      <div class="custom-input-container">
        <i class="italic">W:</i> <input type="text" [(ngModel)]="customs.bannerWidth" name="width" placeholder="width">
        <i class="italic">H:</i> <input type="text" [(ngModel)]="customs.bannerHeight" name="height" placeholder="height">
        <i class="italic">Sec:</i> <input type="text" [(ngModel)]="customs.duration" name="height" placeholder="height">
        <button (click)="videoInput(customs,
                    subCampaign,
                    assetType);
                    customs.bannerWidth=customs.bannerHeight=null"
                class="btn add-custom">Add
        </button>
        <div *ngIf="err.length > 0" class="">
          <div class="custom-input-error">
            {{err}}
          </div>
        </div>
      </div><!--/.custom-input-container-->

      <div *ngFor="let asset of requestItems;">
        <div class="col-sm-3" *ngIf="asset?.custom &&
                    asset?.subcampaign === subCampaign.id && asset?.type + 's' === assetType && asset?.product === productType">
          {{asset.width + ' X ' + asset.height + ' ' + asset.duration}}
          <i class="fa fa-times" aria-hidden="true" (click)="requestsService.removeFromRequestsCollection(asset);
                    asset.selected=false;"></i>
        </div>
      </div><!--/*ngFor-->
  `,
  styles: []
})
export class VideoInputDetailComponent implements OnInit {
  @Input() private productType:string;
  @Input() private assetType:string;
  @Input() private subCampaign:string;
  @Input() private requestItems:any;
  @Output() customVideoSizes:EventEmitter<any> = new EventEmitter();
  private err:string='';
  private _asset:any={};
  private customs:any={};


  constructor(private requestsService:RequestsService) { }

  ngOnInit() {
  }

  videoInput(asset:any, item:any, type:string) {
    this._asset = {
      width: asset.bannerWidth,
      height: asset.bannerHeight,
      duration: asset.duration,
      item: item,
      type: type
    };
    this.customVideoSizes.emit(this._asset);
  }



}
