import { Component, OnInit, Input } from '@angular/core';
import {Campaign} from "../../classes/campaign";
import { CampaignService } from '../../services/campaign.service';
import { Router, ActivatedRoute } from '@angular/router';
import { HttpService } from '../../services/http.service';
import {Order} from "../../classes/order";
import {RequestsService} from "../../services/requests.service";
import {Request} from "../../classes/request";
import {User} from "../../classes/user";
import {UserService} from "../../services/user.service";

@Component({
  selector: 'app-campaign-item',
  templateUrl: './campaign-item.component.html'
})
export class CampaignItemComponent implements OnInit {

  @Input() campaigns;
  private sub: any;
  private campaignId: string;
  private data:any={};
  private selectedAsset:Request;
  private showFrame:boolean;
  private currentSubCampaign:Request;
  private showDownloadForm:boolean=false;
  private productType:string;

  constructor(private campaignService: CampaignService,
              private route: ActivatedRoute,
              private httpService: HttpService,
              private requestService: RequestsService) { }

  /**
   *
   * @param id
     */
  getSelectionFromParams(id) {
    this.httpService.getMethod('campaign', id)
      .subscribe(
        data => {
          this.data = data;
          this.campaigns = this.data.return;
        },
        error => {
          console.log(error);
        }
      );
  }
  ngOnInit() {
    this.productType = 'Origin';
    /**
     * @type {Order[]} main array to store all assets
     **/

    this.sub = this.route
      .params
      .subscribe(params => {
        this.campaignId = params['id'];
      });

    if(typeof this.campaignService.allCampaigns == 'object' && this.campaignService.allCampaigns.length > 0) {
      this.campaigns = this.campaignService.allCampaigns;
      this.campaignId = this.campaignService.selectedCampaign.id;
      this.currentSubCampaign = this.campaignService.selectedCampaign.subcampaigns[0];
    } else {
      this.getSelectionFromParams(this.campaignId);
    }
  }

  /**
   *
   * @param value
     */
  onAssetClicked(value:any) {
    this.showFrame=true;
    this.selectedAsset=value;
  }

  /**
   *
   * @param product
     */
  changeProduct(product) {
    if(product)
      this.productType=product;
  }

}
