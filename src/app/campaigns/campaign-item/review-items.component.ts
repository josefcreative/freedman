import { Component, OnInit, Input } from '@angular/core';
import {RequestsService} from "../../services/requests.service";
import {Request} from "../../classes/request";
import {consoleTestResultHandler} from "tslint/lib/test";
import {Custom} from "../../classes/custom";

@Component({
  selector: 'app-review-items',
  templateUrl: './review-items.component.html',
  styles: []
})
export class ReviewItemsComponent implements OnInit {

  requestItems:Request[]=[];
  customItems:Custom[]=[];

  @Input() subCampaign:any;
  @Input() campaign:any;
  @Input() productType:string;
  private counter:number=0;

  constructor(private requestsService:RequestsService) { }

  ngOnInit() {
    this.requestItems = RequestsService.getArrayOfType(this.requestsService.requestsCollection);
    this.customItems = RequestsService.getArrayOfType(this.requestsService.customsCollection);
  }

  removeRequestFromReview(item:Request, sub:number) {
    this.requestsService.removeFromRequestsCollection(item);
  }


  /**
   *  Order is collected inside the Download Component and passed back to this service
   *  Orders Collection
   */


}
