import { Component, OnInit, Input } from '@angular/core';
import { Campaign  } from '../../classes/campaign';
import {HttpService} from "../../services/http.service";
import {CampaignService} from "../../services/campaign.service";

@Component({
  selector: 'app-campaign-select',
  templateUrl: './campaign-select.component.html',
  styleUrls: ['./campaign-select.component.css']
})
export class CampaignSelectComponent implements OnInit {

  @Input() campaigns: Campaign;
  private data;
  campaignSelected=false;

  constructor(private httpService: HttpService, private campaignService: CampaignService) { }

  ngOnInit() {
    this.getAllCampaigns();
  }

  getAllCampaigns() {
    this.httpService.getMethod('campaign','')
      .subscribe(
        data => {
          this.data = data;
          this.campaigns = this.data.return;
        }
      )
  }

  selectCampaign(campaign) {
    if(campaign){
      this.campaignService.selectedCampaign = campaign;
    }
  }

}
