/**
 * Created by josefgasewicz on 17/10/2016.
 */
import { Routes } from "@angular/router";

import {CampaignItemComponent} from "./campaign-item/campaign-item.component";
import {CampaignSelectComponent} from "./campaign-select/campaign-select.component";

import {AuthGuard} from "../_guards/auth.guard";

export const CAMPAIGN_ROUTES: Routes = [
  { path: '', component: CampaignSelectComponent },
  { path: ':id', component: CampaignItemComponent, canActivate: [AuthGuard] }
];
