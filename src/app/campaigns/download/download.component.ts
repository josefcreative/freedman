import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators} from '@angular/forms';
import {RequestsService} from "../../services/requests.service";
import {Order} from "../../classes/order";
import {Request} from "../../classes/request";
import {Custom} from "../../classes/custom";
import {HttpService} from "../../services/http.service";
import {Campaign} from "../../classes/campaign";

@Component({
  selector: 'app-download',
  templateUrl: './download.component.html'
})
export class DownloadComponent implements OnInit {

  @Input() showDownload:boolean;
  @Input() campaign:Campaign;
  dLForm: FormGroup;
  private order: Order;
  private requests:Request[];

  constructor(private requestsService: RequestsService, private httpService:HttpService) {
    this.dLForm = new FormGroup({
      'market': new FormControl('',
        [Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),]),
      'deadline': new FormControl('',
        [Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),]),
      'live': new FormControl('',
        [Validators.required,
          Validators.minLength(3),
          Validators.maxLength(10),]),
      'localization': new FormControl('',
        [Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),]),
      'translation': new FormControl('',
        [Validators.required,
          Validators.minLength(2),
          Validators.maxLength(10),])
    });
  }

  ngOnInit() {
    this.requests = RequestsService.getArrayOfType(this.requestsService.requestsCollection);
  }

  onSubmit() {

      let assets:Request[]=[];
      let customs:Custom[]=[];
      for(var i=0; i<this.requests.length; i++) {
        this.requests[i].asset = this.requests[i].id;
        delete this.requests[i]['selected'];
        if(this.requests[i].custom) customs.push(this.requests[i]);
        if(!this.requests[i].custom) assets.push(this.requests[i]);
      }
      this.order = this.dLForm.value;
      this.order.campaign = this.campaign.id;
      let formData = new FormData();
      formData.append("order", JSON.stringify(this.order));
      formData.append("assets", JSON.stringify(assets));
      formData.append("customs", JSON.stringify(customs));
    console.log(formData);
    console.log({
      orders : this.order,
      assets : assets,
      customs : customs
    });
      this.httpService.postMethod('order', '', formData)
        .subscribe(
          data => {
            console.log(data);
          }
        )
    }



}
