import { Component, OnInit, Input } from '@angular/core';
import {RequestsService} from "../../services/requests.service";

@Component({
  selector: 'app-frame',
  templateUrl: './frame.component.html',
  styles: []
})
export class FrameComponent implements OnInit {

  @Input() currentAsset;
  @Input() showFrame:boolean;

  constructor(private requestService: RequestsService) { }

  ngOnInit() {
  }

}
