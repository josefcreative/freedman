export class Campaign {
  id: number;
  managed: boolean;
  manager: string;
  name: string;
  placeholder: boolean;
  campaignAsset: [{
      assetType: string,
      name: string,
      placeholder: string,
      id: string
    }];
}
