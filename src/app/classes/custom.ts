export class Custom {
  public subcampaign:number; // subcampaign id
  public asset:number;
  public type:string;
  public width:number;
  public height:number;
  public duration:number;
}
