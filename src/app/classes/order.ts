import { Request } from './request';
import { Custom } from './custom';

export class Order {
  public campaign:number;
  public market:string;
  public deadline:string;
  public live:string;
  public localization:boolean;
  public translation:boolean;
  public assets:Request[];
  public custom:Custom[];
}
