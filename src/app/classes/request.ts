/**
 * Request is an asset type, ie a banner, video pr a skin
 * Warning: Subcampaign class also uses type Request
 */
export class Request {
  public id:number;
  public subcampaign:number;
  public asset:number;
  public custom:boolean;
  public type:string;
  public width:number;
  public height:number;
  public duration:number;
  public product:string;
}
