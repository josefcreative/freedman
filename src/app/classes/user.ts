export class User {
  public id: string;
  public name: string;
  public surname: string;
  public market: string;
  public password: string;
  public email: string;
  public company: string;
  public admin: boolean;
}
