import {Directive, Output, ElementRef, Renderer, HostListener, HostBinding, EventEmitter, Input} from '@angular/core';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {HttpService} from "../services/http.service";


@Directive({
  selector: '[appDragDrop]'
})
export class DragDropDirective {

  public droppedFiles;
  @Output() fileEvent = new EventEmitter<any>();

  @HostListener('drop', ['$event']) mouseover(e) {
    e.preventDefault();
    e.stopPropagation();
    this.droppedFiles = e.dataTransfer.files[0];
    this.fileEvent.emit(this.droppedFiles);
  }

  @HostListener('mouseenter', ['$event']) hoverOver(e) {
    console.log('hover');
  }

  constructor(private elementRef: ElementRef, private renderer: Renderer, private httpService: HttpService) {
  }
}
