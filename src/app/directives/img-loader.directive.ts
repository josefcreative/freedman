import { Directive, ElementRef, Renderer, OnInit} from '@angular/core';

@Directive({
  selector: '[imgLoader]',
  host: { '(load)':'onLoad()' }
})

export class ImgLoaderDirective implements OnInit{
  private Element: any;

  constructor(private elRef:ElementRef, private renderer:Renderer){}

  ngOnInit() {
    this.Element = this.elRef.nativeElement;
    this.renderer.setElementStyle(this.Element, 'opacity', '0.3');
    this.renderer.setElementStyle(this.Element, 'transform-origin', '0 0');
    this.renderer.setElementStyle(this.Element, 'transform', 'scale(1,0)');
  }

  onLoad()
  {

    this.renderer.invokeElementMethod(
      this.Element,
      'animate',
      [
        [
          {transform: 'scale(1,0)', opacity: '0'},
          {transform: 'scale(1,1)', opacity: '1'}
        ],
        {
          duration: 1200,
          delay: 0,
          fill: 'forwards'
        }
      ]
    );
  }
}
