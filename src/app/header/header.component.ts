import { Component, OnInit, Input} from '@angular/core';
import { Http, Response } from '@angular/http';
import { HttpService } from '../services/http.service';
import { AuthenticationService } from '../services/auth.service'
import {UserService} from "../services/user.service";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent {
  @Input() user:any={};

  constructor() { }

  ngOnInit() {
  }

}
