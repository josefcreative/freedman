import { Component, OnInit } from '@angular/core';
import { User } from './../classes/user';
import { Http, Response } from '@angular/http';
import { HttpService } from '../services/http.service';
import { Router } from '@angular/router';
import {AuthenticationService} from "../services/auth.service";
import {UserService} from "../services/user.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {


  model: any= {};
  private preloader: boolean = false;
  private errors;
  private showResetForm: boolean = false;
  private showLoginForm: boolean = true;

  constructor(private user: User,
              private router: Router,
              private httpService: HttpService,
              private authenticationService: AuthenticationService,
              private userService:UserService) { }
  ngOnInit() {
  }
  login() {
    this.preloader = true;
    var formData = new FormData();
    formData.append("email", this.model.email);
    formData.append("password", this.model.password);
    formData.append("memorize", this.model.memorize);
    this.authenticationService.login(formData)
      .subscribe(
          response => {
            let data = response;
            this.preloader = false;
            if(!data.result) {
              this.errors = data;
              return;
            }
            if(data.result){
              this.authenticationService.setToken(data.authentication);
              this.userService.shareUser(data.authentication.details);
              this.router.navigate(['/campaigns']);
            }
          }
      )
  }

  showReset() {
    this.showLoginForm=false;
    this.showResetForm=true;
  }
}
