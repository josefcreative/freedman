import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-reset',
  template: `
        <form class="base-form log-form" name="form" (ngSubmit)="resetF.form.valid && reset()" #resetF="ngForm" novalidate>
          <p class="text-center">Reset your password</p>
          <div class="form-group">
            <input class="form-control"
            #email="ngModel"
            placeholder="Enter your email"
            name="email"
            [(ngModel)]="resetForm.email"
            required
            pattern="[emailPattern]"
            >
            <i class="fa fa-user" aria-hidden="true"></i>
          </div>
          <button type="submit" class="btn btn-sub">Submit</button>
        </form>
  `,
  styles: []
})
export class ResetComponent implements OnInit {
  private resetForm: any={};
  emailPatter = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
  constructor() { }

  ngOnInit() {
  }

  reset() {
    let formData = new FormData();
    formData.append('email', this.resetForm.email);
    // TODO reset password
  }

}
