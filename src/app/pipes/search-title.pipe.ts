import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchTitle'
})
export class SearchTitlePipe implements PipeTransform {

  transform(value: any, args?: any): any {
    if(typeof value === 'object'){
      if(value.length === 0) {
        return value;
      }
      let resultArray = [];
      for(let item of value) {
        if(item['title'].match(new RegExp(''+args, 'i'))) {
          resultArray.push(item);
        }
      }
      return resultArray;
    }
  }

}
