import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { HttpService } from '../services/http.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {



  title: string = 'Register';
  registerForm: FormGroup;
  registerMessage: string;

  constructor(private httpService: HttpService, private router: Router) {
    this.registerForm = new FormGroup({
      'name': new FormControl('', [
        Validators.minLength(3),
        Validators.maxLength(10),
        Validators.required]),
      'surname': new FormControl('', [
        Validators.minLength(3),
        Validators.maxLength(10),
        Validators.required]),
      'email': new FormControl('', [
        Validators.required,
        Validators.pattern("[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")]),
      'password': new FormControl('',[
        Validators.minLength(8),
        Validators.required]),
      'company': new FormControl('', [
        Validators.minLength(3),
        Validators.maxLength(10),
        Validators.required]),
      'market': new FormControl('', [
        Validators.minLength(3),
        Validators.maxLength(20)]),
      'terms': new FormControl(Validators.required)
    });
  }

  private newUser;

  onSubmit() {
    this.newUser = this.registerForm.value;
    let _formData = new FormData();
    _formData.append("email", this.newUser.email);
    _formData.append("password", this.newUser.password);
    _formData.append("name", this.newUser.name);
    _formData.append("company", this.newUser.company);
    _formData.append("surname", this.newUser.surname);
    this.httpService.registerUser(_formData)
      .subscribe(
        data => {
          this.registerMessage = data;
          console.log('this.registerMessage: ' + this.registerMessage);
          this.router.navigate(['/login']);
        }
      )
  }

  ngOnInit() {

  }

}
