import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Router, NavigationStart } from '@angular/router';
import { Observable, Subject } from 'rxjs/Rx';
import {FileUploadService} from "./file-upload.service";
import 'rxjs/add/operator/map';


@Injectable()
  export class AuthenticationService {
    public isLoggedIn: boolean;
    public token: string;
    public _url: string = 'http://gameyfi.com/lab/ea/api/';
    public validationMessages:any={};

    constructor(private http: Http) {

      let currentUser = JSON.parse(localStorage.getItem('currentUser'));
      this.token = currentUser && currentUser.token;

      this.validationMessages = {
        'name': {
          'required': 'Name is required.',
          'minlength': 'Name must be at least 3 characters long.',
          'maxlength': 'Name cannot be more than 24 characters long.'
        },
        'surname': {
          'required': 'Surname is required.',
          'minlength': 'Surname must be at least 3 characters long.',
          'maxlength': 'Surname cannot be more than 24 characters long.'
        },
        'password': {
          'minlength': 'Password must be at least 8 characters long.',
          'maxlength': 'Password cannot be more than 24 characters long.'
        },
        'market': {
          'required': 'Market is required.',
          'minlength': 'Market must be at least 3 characters long.',
          'maxlength': 'Market cannot be more than 24 characters long.'
        },
        'company': {
          'required': 'Company is required.',
          'minlength': 'Company must be at least 3 characters long.',
          'maxlength': 'Company cannot be more than 24 characters long.'
        }


      }
    }


  login(formData): Observable<any> {
    return this.http.post(this._url + 'login', formData)
      .map((response: Response) => response.json()); // TODO try catch and create a method to splice off the dump from beginning of object
  }

  logOut(): void {
    this.token = null;
    localStorage.removeItem('currentUser');
  }

  public createHeaders(url:boolean) {
  //  console.log('headers updated: ' + this.token);
    let headers = new Headers();
    if(url || url != null) headers.append('Content-Type', 'application/x-www-form-urlencoded');
    headers.append('Authorization', 'Bearer ' + this.token);
    return new RequestOptions({headers: headers});
  }

  public setToken(authentication) {
    if(typeof authentication.token != 'undefined' && authentication.token !== null && authentication.token.length > 0) {
      this.token = authentication.token;
      localStorage.setItem('currentUser', JSON.stringify({token: this.token}));
      FileUploadService.SetToken(this.token);
     // console.log('token updated: ' + this.token);
    }
  }
  public static getToken() {
    let token;
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    token = currentUser && currentUser.token;
    return token;
  }

  public static checkValIsNumber(val:any):boolean{
    if(val == null || val == "undefined" || isNaN(val) || val.length > 5) return false;
    return true;
  }
}
