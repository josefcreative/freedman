import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs/Rx';
import {AuthenticationService} from "./auth.service";

@Injectable()
export class FileUploadService {
  // public progress: Observable<any>;
  //  	private progressObserver: Observer<number>;

  private static url: string;
  private static token: string;
  private static busy:boolean = false;
  private static progress:number = 0;

  constructor() {

  }

  public static Available()
  {
    return FileUploadService.busy ? false : true;
  }

  public static GetProgress()
  {
    let currentProgress: number = FileUploadService.progress;
    return currentProgress;
  }

  public static SetURL(url: string)
  {
    FileUploadService.url = url;
  }



  public static SetToken(token: string)
  {
    FileUploadService.token = token;
  }

  public static UploadFile (file: File): Observable<any>
  {
    FileUploadService.token = AuthenticationService.getToken();
    FileUploadService.busy = true;
    return Observable.create(observer => {
      let formData: FormData = new FormData(),
        xhr: XMLHttpRequest = new XMLHttpRequest();

      formData.append("file", file, file.name);

      xhr.onreadystatechange = () => {
        console.log(xhr.response);
        if (xhr.readyState === 4) {
          FileUploadService.busy = false;
          FileUploadService.progress = 0;
          observer.next([xhr.status,xhr.response]);
          observer.complete();
        }
      };

      xhr.upload.onprogress = (event) => {
        var progress = parseFloat((event.loaded / event.total * 100).toFixed(2));
        // this.progressObserver.next(progress);
        FileUploadService.progress = progress;
      };
      FileUploadService.SetURL('http://gameyfi.com/lab/ea/api/upload');
      console.log('FileUploadService.url ' + FileUploadService.url);

      xhr.open('POST', FileUploadService.url, true);
      xhr.setRequestHeader('Authorization','Bearer '+FileUploadService.token);
      xhr.send(formData);

    });
  }
}
