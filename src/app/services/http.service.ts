import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response} from '@angular/http';
import { Observable, Subject } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { AuthenticationService } from '../services/auth.service';

@Injectable()
export class HttpService {
  appData:any={};

  public url: string = 'http://gameyfi.com/lab/ea/api/';

  constructor(private http: Http, private authenticationService: AuthenticationService) {
  }

  public registerUser(formData): Observable<any> {
    let headers = new Headers;
    headers.append('Content-Type', 'multipart/form-data');
    return this.http.post(this.url + 'user', formData)
      .map((response: Response) => response.json());
  }

  public getMethod(endpoint: string, noun: string): Observable<any> {
 console.log(endpoint + noun);
    let url = this.url + endpoint;
    if(typeof noun != 'undefined' && noun.length)
        url += '/'+noun;
    console.log(url);
    let options = this.authenticationService.createHeaders(null);
    let theObs = this.http.get(url, options).map((res: Response) => res.json()); // TODO delete ?bypass=admin
    theObs.subscribe(
      res => {
      this.appData = res;
      this.authenticationService.setToken(this.appData.authentication)
      },
      error => {
        console.error(error);
      }
    );


    return theObs;
  }

  public postMethod(endpoint: string, noun: string, obj): Observable<any> {
    console.log(endpoint + noun);
    let url = this.url + endpoint;
    if(typeof noun != 'undefined' && noun.length)
      url += '/'+noun;
    console.log('post endpoint: ' +url);
    let options = this.authenticationService.createHeaders(null);
    let theObs = this.http.post(url, obj, options); // TODO delete ?bypass=admin
    theObs.subscribe(
      res => {
        this.appData = res;
      this.authenticationService.setToken(this.appData.authentication)
      },
      error => {
      console.error(error);
    }
    );
    return theObs;
  }



  public putMethod(endpoint: string, noun: string, obj): Observable<any> {
    console.log(endpoint + noun);
    let url = this.url + endpoint;
    if(typeof noun != 'undefined' && noun.length)
      url += '/'+noun;
    console.log('post endpoint: ' +url);
    let options = this.authenticationService.createHeaders(true);
    let theObs = this.http.put(url, obj, options); // TODO delete ?bypass=admin
    theObs.subscribe(
      res => {
        this.appData = res;
        this.authenticationService.setToken(this.appData.authentication)
      },
      error => {
        console.error(error);
      }
    );
    return theObs;
  }

  public patchMethod(endpoint: string, noun: string, obj): Observable<any> {
    console.log(endpoint + noun);
    let url = this.url + endpoint;
    if(typeof noun != 'undefined' && noun.length)
      url += '/'+noun;
    console.log('post endpoint: ' +url);
    let options = this.authenticationService.createHeaders(true);
    let theObs = this.http.patch(url, obj, options); // TODO delete ?bypass=admin
    theObs.subscribe(
      res => {
        this.appData = res;
        this.authenticationService.setToken(this.appData.authentication)
      },
      error => {
        console.error(error);
      }
    );
    return theObs;
  }

  /**
   * TEST
   */

  public testDropPost(data) {
    let headers = new  Headers();
    headers.append('Content-Type', 'multipart/form-data');
    return this.http.post('http://localhost:3000/api/drop', data, headers)
      .map((response: Response) => response.json());
  }
}

