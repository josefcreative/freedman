import { Injectable } from '@angular/core';
import {Order} from "../classes/order";
import {Request} from "../classes/request";
import {Custom} from "../classes/custom";
import {HttpService} from "./http.service";

@Injectable()
export class RequestsService {

  public customs:Custom[] = [];
  public requestsCollection:Request[]=[];
  public customsCollection:Custom[]=[];
  public selectAllBanners:boolean=false;
  public selectAllVideos:boolean=false;
  public requestsError:string=null;
  public displayDownloadsFormMask:boolean;
  public order:Order;
  public toggleBanner:boolean=false;
  public toggleVideo:boolean=false;

  constructor(private httpService:HttpService) { }

  /**
   *
   * @param type
   * @returns {any[]}
     */
  public static getArrayOfType(type:any) {
    return type;
  }

  /**
   *
   * @param item
   * @param type 'video' || 'banner'
   * @param collection
     */
  public static addToArrayCollection(item:any, type:string, collection:any[]) {
    if(collection.indexOf(item) !== -1) {
      return;
    }
    collection.push(item);
  }

  /**
   *
   * @param item
     */
  removeFromRequestsCollection(item:Request) {
    this.requestsCollection.splice(this.requestsCollection.indexOf(item), 1);
  }

  /**
   *
   * @param request
     */
  postRequests(request:Request[]) {
    if(request.length < 1) {
      this.requestsError = "You need to add at least 1 item to your order";
      setTimeout(()=>{
        this.requestsError = null;
      }, 4000)
    }
    if(request.length >= 1) {
      this.requestsError = null;
    }
  }

  /**
   *
   * @param collection
   * @param productType
   * @param id
     */
  public toggleOnTypeChange(collection:any, productType:string, id:number):void {
    let bannerCounter=0;
    let videoCounter=0;
    for(let i=0; i<collection.length; i++) {
      if(collection[i].product === productType && collection[i].subcampaign===id) {
        if(collection[i].type ==='banner') bannerCounter ++;
        if(collection[i].type ==='video') videoCounter ++;
      }
    }
    (bannerCounter >= 1) ? this.toggleBanner=true :this.toggleBanner=false;
    (videoCounter >= 1) ? this.toggleVideo=true :this.toggleVideo=false;
  }

}
