import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {setInterval} from "timers";
import {Subcampaign} from "../classes/subcampaign";
import {Request} from "../classes/request";

@Injectable()
export class SubCampaignUploadService {

  private progress$:Observable<any>;
  private progressValue:number=0;
  private progressObserver:any;
  public subCollection:Subcampaign[]=[];

  constructor() {
    this.progress$ = new Observable(observer => {
      this.progressObserver=observer
    });
  }

  /**
   *
   * @returns {Observable<any>}
   */
  public getObserver():Observable<any> {
    return this.progress$;
  }

  /**
   *
   * @param file
   * @returns {any}
   */
   public upload(file) {
    return Observable.fromPromise(new Promise((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.onreadystatechange = function () {
        if (xhr.readyState === 4) {
          if (xhr.status === 200) {
            resolve(JSON.parse(xhr.response))
          } else {
            reject(xhr.response);
          }
        }
      };

      setInterval(()=>{}, 500);
      xhr.upload.onprogress = (event) => {
        this.progressValue = Math.round(event.loaded / event.total * 100);
        this.progressObserver.next(this.progressValue);

      };

      xhr.open("POST", "http://localhost:3000/api/drop", true);
      xhr.send(file);
    }))

  }


  /**
   *
   * @param arr
   * @returns {Subcampaign[]}
   */
  public getSubArray(arr:Subcampaign[]):any {
    return arr;
  }

  /**
   *
   * @param item
   * @param collection
   */
  public addToArrayCollection(item, collection):void {
    if(collection.indexOf(item) !== -1) {
      return;
    }
    collection.push(item);
  }

  /**
   *
   * @param item
   */
  public removeItemFromCollection(item, collection):void {
    collection.splice(collection.indexOf(item), 1);
  }

  /**
   *
   * @param obj
   * @param prop
   * @param value
   * @returns {any}
   */
  public addPropToObj(obj, prop, value):any {
    if(obj.hasOwnProperty(prop)) return;
    obj[prop]=value;
    return obj;
  }

  /**
   *  Ths method ads a product type to the subCollection on radio btn selection
   * @param filename
   * @param product
   * @param collection
   */
  public addProduct(filename, product, collection):void {

    console.log('first' + filename);
    for(let i=0;i<collection.length;i++) {
      if(collection[i].filename===filename) {
        collection[i].product=product;
      }
    }
  }
}
