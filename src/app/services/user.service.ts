import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import {Subject} from "rxjs/Subject";

@Injectable()
export class UserService {

  public user:any;
  private userInit: Subject<any> = new Subject<any>();
  userInit$ = this.userInit.asObservable();

  constructor() {
    this.user = {};
  }

  public initUser(user:any) {
     this.user = user;
  }
  shareUser(user) {
    this.user = user;
    this.userInit.next(user);
  }

  public getUserObj() {
    return this.user;
  }

}

