import { Component, OnInit } from '@angular/core';
import {Subscription} from "rxjs/Subscription";
import {UserService} from "../../services/user.service";
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-user-requests',
  templateUrl: './user-requests.component.html',
  styleUrls: ['./user-requests.component.css']
})
export class UserRequestsComponent implements OnInit {

  private ordersCollection;
  private user;
  private _sub;
  private subscription: Subscription;
  private showBody:any;

  constructor(private userService:UserService,
              private httpService:HttpService) {

    this.user = this.userService.user;
    this.subscription = userService.userInit$.subscribe(
      user => {
        this.user = user;
        console.log(this.user);
      }
    );
  }

  ngOnInit() {
    this.getOrdersCollection();
    console.log(this.ordersCollection);
  }

  getOrdersCollection() {
    this.httpService.getMethod('order', '')
      .subscribe(
        data => {
          console.log(data.return);
          this.ordersCollection = data.return;
        },
        error => {
          console.log(error);
        }
      )
  }
  onClickShowMore(value) {
    this.showBody = value;
    console.log(this.showBody);
  }

}
