import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { URLSearchParams } from '@angular/http';
import {HttpService} from "../../services/http.service";
import {UserService} from "../../services/user.service";
import {Subscription} from "rxjs/Subscription";
import {AuthenticationService} from "../../services/auth.service";

@Component({
  selector: 'app-user-settings',
  templateUrl: './user-settings.component.html',
  styleUrls: ['./user-settings.component.css']
})
export class UserSettingsComponent implements OnInit {

  private userSettingsForm:FormGroup;
  private user;
  private _sub;
  private subscription: Subscription;

  constructor(private httpService:HttpService,
              private userService:UserService) {

    this.user = this.userService.user;
    this.subscription = userService.userInit$.subscribe(
      user => {
        this.user = user;
      }
    );

  }

  ngOnInit() {
    this.userSettingsForm = new FormGroup({
      'name': new FormControl(this.user.name, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)
      ]),
      'surname': new FormControl(this.user.surname, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(20)
      ]),
      'password': new FormControl(this.user.password, [
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(20)
      ]),
      'market': new FormControl(this.user.market, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)
      ]),
      'company': new FormControl(this.user.company, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(20)
      ])
    });
  }

  onSettingsFormSubmit():any{
    let urlSearchParams = new URLSearchParams();
    urlSearchParams.append('name', this.userSettingsForm.value.name);
    urlSearchParams.append('surname', this.userSettingsForm.value.surname);
    if(this.userSettingsForm.value.password !== null) {
      if(this.userSettingsForm.value.password.length > 7) urlSearchParams.append('password', this.userSettingsForm.value.password);
    }
    urlSearchParams.append('market', this.userSettingsForm.value.market);
    urlSearchParams.append('company', this.userSettingsForm.value.company);
    return urlSearchParams;
  }

  patchSettingsForm(e) {
    e.preventDefault();
    this.httpService.patchMethod('user', this.user.id, this.onSettingsFormSubmit())
      .subscribe(
        data => {
          console.log(data);
        },
        error => {
          console.log(error);
        }
      )
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
